# Betty Profiles
I hate waiting. Especially when I’m trying to get something done, like when I’m troubleshooting a slow application for my customer. I don’t have time to set everything up in Wireshark to optimize it for the situation. I need it to be ready to go right now.

That’s why I customize my profiles. I set up my default profile to be exactly what I need as a starting point. Wireshark is over 20 years old, and at the time the defaults that were chosen made perfect sense. We were just excited to be able to sniff packets for free. Times have changed and so have our needs.

I tweaked my default profile to be how I need it to be. It might not be perfect for you, but I bet it will be a good starting point. I copy mine and use it as the base for all my profiles.

## What do I change?

### Screen Layout
Note: Starting with Wireshark 4.0, the split screen at the bottom is the default.

First thing I always change is the layout. I use the split screen at the bottom of the screen most of the time. If I’m using a large monitor, then I like to have the Detail and Bytes panes vertical. My two favorite layouts are in the screenshot below.


![Wireshark Preferences-Appearance](profiles-betty/images/DefaultLayout.png)

### Color Rules
I have a method to my color rules that I learned from Laura Chappell. In the past, I had a different background and text combo for each color rule. That got unmanageable pretty quickly, trying to use different shades of red, yellow and green to signify all possibilities. Laura categorizes her rules into security, troubleshooting and notes to self. Each category is a different color, so much easier to remember. When I see anything red, I know it is a troubleshooting issue. I added other categories, V-VoIP, 2-7 for layers of the OSI model, and R-routing. (my routing color rules only show up in my routing profile though)

I use the same color rule set for all of my profiles with the exception of those I use when I’m in Dark mode.

![Betty DuBois' Wireshark Coloring Rules](profiles-betty/images/DefaultColors.png)



## Display Filter Buttons
Thanks to Filter Button Groups a/k/a FBGs, I can group my filters. Now I use the same filter set in my “up through layer 4 profiles”.  They are grouped by OSI Model layer, Frame (metadata), and General. Now I have to work on my Layer7 filter set for the rest of my profiles.

![Betty DuBois’ Filter Button Groups - FBGs](profiles-betty/images/FBGs.png)


## Columns
I’ll be the first to say it, I am a column freak. Most of my profiles have more columns than anyone expects, but I find it easier to look at things in the packet list pane than in the detail pane. My rule of thumb is if I have scrolled through the details more than 3 times, it becomes a column. Plus, now that drag and drop filters make our lives easier, it is a lot faster to drag and drop from the packet list than from the detail.


## Best for Last - Enabled Protocols
Remember how I said, I hate waiting? Well, waiting for a file to open or a filter to apply likes to kill me. Well what makes the biggest difference in how quickly a pcap can be worked with is how many protocol dissectors are loaded. So I just turn off what I don’t need, and turn them back on a per profile basis. Now I measure pcaps for Wireshark in gigs, not megs. Here is what I have still on (Analyze | Enabled Protocols).

I pulled out as many as possible to make Wireshark faster.  If something isn’t dissecting (it only shows as Data in the details), flip to the Wireshark Default profile and see if it now dissects.  Add that protocol in Analyze>Enabled Protocols.

Each new version of Wireshark adds new dissector files, so I have to keep going back in and deselecting the new ones.

These protocols (plus a few more) are on in these profiles.  Ethernet, Raw, STP, VLAN, VTP, GRE, CDP, IP, IP6, ICMP,ICMP6, DHCP, RIP, EIGRP, OSPF, VXLAN, UDP, TCP, DNS, TLS, HTTP1|2|3, SMB, SMB2, DECRPC, RPC, RTP, all the VoIPs, and NFS.

If you would like to use profiles, feel free to download the entire directory and import it into Wireshark. In the menu, go to Edit>Configuration Profiles>Import From a Zipfile.

Download and use at your own risk, I make no warranties or guaranties.
