# Betty-Default

I build all of my other profiles from this one. 

In Wireshark, go to Edit>Configuration Profiles>Import from Directory>Browse to wherever you saved the directory

## What did I change?

### Color rules
I standardized my color rules, and use the same set for all of my profiles.  Red is for troubleshooting, butter yellow is for notice but don’t freak out, and white backgrounds are delineated by the color for each layer of the OSI model.

### Display Filter Buttons
Thanks to Filter Button Groups a/k/a FBGs, I can group my filters. Now I use the same filter set in my “up through layer 4” profiles.  They are grouped by OSI Model layer, Frame (metadata), and General. Now I have to work on my Layer7 filter set for the rest of my profiles.

### Columns
I added Delta Time Displayed (time from the end of a packet to the next one that displays through the filter), Delta Time Application (same thing, but from an Application layer request to response packet), which color rule applies,TCP initial round trip time, Delta Time TCP (same as Delta Displayed but for a single stream without having to filter), and UDP or TCP stream #. 

### Enabled Protocols
I pulled out as many as possible to make Wireshark faster.  If something isn’t dissecting (it only shows as Data in the details), flip to the Wireshark Default profile and see if it now dissects.  Add that protocol in Analyze>Enabled Protocols.

Each new version of Wireshark adds new dissector files, so I have to keep going back in and deselecting the new ones.

These protocols (plus a few more) are on in this profile.  Ethernet, Raw, STP, VLAN, VTP, GRE, CDP, IP, IP6, ICMP,ICMP6, DHCP, RIP, EIGRP, OSPF, VXLAN, UDP, TCP, DNS, TLS, HTTP1|2|3, SMB, SMB2, DECRPC, RPC, RTP, all the VoIPs, and NFS.

Download and use at your own risk, I make no warranties or guaranties.