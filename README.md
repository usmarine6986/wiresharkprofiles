# Wireshark Profiles Repository - Analyze Pcaps Faster 
## _**Updated for 4.x Syntax**._ Check the [Wiki](https://gitlab.com/BettyD/wiresharkprofiles/-/wikis/home) for project description.

![Woman At Aquarium](profiles-betty/images/ian-schneider-unsplash-20.jpg)


### Have Wireshark ready when you need it the most.

Use the whole profile or just the parts you need. Flexible yet consistent.

# WHAT'S IN A WIRESHARK PROFILE?

Text files that change how Wireshark works. You can think of them as a template or setup files. 

### PREFERENCES
Change how Wireshark looks, feels, and acts. There are nine sets of options. Make choices that work best for you.

### COLOR RULES
Make your packets jump out at you. Or toggle off the colors, and use the rule names as another column, sorting, or filtering opportunity.

### FILTERS
Life is too short to look at **all** of the packets.  Using filters saves time, saving filters in a profile - even more time.

# HOW BEST TO USE THEM?

### SHARING
It is as easy as import/export. Or just "copy from" certain settings including color rules and filter buttons.

### BY PURPOSE OR PROTOCOL?
Either. You can have profiles for a specific purpose such as security incident or by protocol. Some like to have both. There are no wrong answers, just personal preferences.


### Watch how to quickly [import profiles into Wireshark](https://youtu.be/cwOI970jhuI). 

These profiles are provided with neither warranty nor guarantee.  Use at your **own** risk.


Photo by [Ian Schneider](https://unsplash.com/@goian?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on Unsplash
